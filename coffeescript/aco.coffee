class aco
    @nodeList
    @edgeList
    @edgeLengthList
    @feromoneTable
    @adjList
    @sigmaElementId
    @sigInst
    @forceAtlasStarted
    @startPointSelectionStarted
    @endPointSelectionStarted

    @hookBeforeForceAtlas
    @hookAfterForceAtlas

    @hookAfterInvokeStartForceAtlas
    @hookAfterInvokeStopForceAtlas

    @startPoint = null
    @endPoint = null

    @highchartContainer = null
    click: (e) ->
        if @startPointSelectionStarted
            @startPoint = e.content[0]
        else if @endPointSelectionStarted
            @endPoint = e.content[0]
        return

    constructor: (sigEl = 'sigInst',highchartContainer = 'container',forceAtlasStarted = true) ->
      @sigmaElementId = sigEl
      @sigInst = sigma.init(document.getElementById @sigmaElementId).drawingProperties {
        defaultLabelColor: '#fff'
      }
      @nodeList = new Array
      @edgeList = new Array
      @edgeLengthList = new Array
      @adjList = new Array
      @forceAtlasStarted = forceAtlasStarted
      @startPointSelectionStarted = false
      @endPointSelectionStarted = false
      @feromoneTable = new Array
      
      @startPoint = null
      @endPoint = null
      @highchartContainer = new Highcharts.Chart {
        title: {
            text: 'Ant Colony Optimization',
            x: -20
        },
        subtitle: {
            text: 'Długość ścieżki jaką pokonują mrówki',
            x: -20
        },
        xAxis: {
            title: {
              text: 'Iteracja'
            },
            categories: [],
        },
        scrollbar: {
          enabled : true,
          liveRedraw: true 
        }
        yAxis: {
            title: {
                text: 'Długość ścieżki'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }],
            min: 0
        },
        chart: {
          renderTo : document.getElementById(highchartContainer),
          zoomType: 'xy'
        }
      }
      

    getSigmaInstance :->
        @sigInst
    setAfterInvokeStartForceAtlas: (func)->
      @hookAfterInvokeStartForceAtlas = func
      return

    setAfterInvokeStopForceAtlas: (func)->
      @hookAfterInvokeStopForceAtlas = func
      return

    setBeforeForceAtlas: (func)->
      @hookBeforeForceAtlas = func
      return
    
    setAfterForceAtlas: (func)->
      @hookAfterForceAtlas = func
      return

    isForceAtlasRunning :->
      @forceAtlasStarted

    enableStartPointSelection :->
      @startPointSelectionStarted = true
      @endPointSelectionStarted = false if @endPointSelectionStarted
      this.stopForceAtlas() if @forceAtlasStarted
      return

    disableStartPointSelection :->
      @startPointSelectionStarted = false
      return

    statusStartPointSelection :->
      @startPointSelectionStarted

    enableEndPointSelection :->
      @endPointSelectionStarted = true
      @startPointSelectionStarted = false if @startPointSelectionStarted
      this.stopForceAtlas() if @forceAtlasStarted
      return

    disableEndPointSelection :->
      @endPointSelectionStarted = false
      return

    statusEndPointSelection :->
      @endPointSelectionStarted

    getStartNode :->
      @startPoint

    getEndNode :->
      @endPoint

    setStartNode: (node)->
      @startPoint = node

    setEndNode: (node)->
      @endPoint = node

    rescaleGraph :->
      @sigInst.position(0,0,1).draw()
    
    recalculateEdgeValue: ->
      for edge in @edgeList
        @edgeLengthList[edge.id] = Math.sqrt( Math.pow(@nodeList[edge.source].x - @nodeList[edge.target].x,2)+Math.pow(@nodeList[edge.source].y - @nodeList[edge.target].y,2))
      return

    antWalk:(feromoneInfluence = 3, distanceInfluence = 1)->
        nodesVisited = new Array
        edgeVisited = new Array
        currentPoint = @startPoint
        loop
            connectedEdges = new Array
            propability = new Array
            nextPoint = null

            for edge in @edgeList
                if edge.source is currentPoint or edge.target is currentPoint
                    if edge.source is currentPoint and nodesVisited.indexOf(edge.target) is -1
                        connectedEdges.push edge
                    else if edge.target is currentPoint and nodesVisited.indexOf(edge.source) is -1
                        connectedEdges.push edge
            
            sumOfPropability = 0
            for edge in connectedEdges
                sumOfPropability +=  Math.pow(@feromoneTable[edge.id],feromoneInfluence) * Math.pow(1/@edgeLengthList[edge.id], distanceInfluence)
            
            randomEdge = null

            if sumOfPropability isnt 0 or connectedEdges.length is 1
                randomValue = Math.floor(Math.random()*101);
                if connectedEdges.length isnt 1
                    
                    for edge in connectedEdges
                        test_prop = Math.floor(((Math.pow(@feromoneTable[edge.id],feromoneInfluence) * Math.pow(1/@edgeLengthList[edge.id], distanceInfluence))/sumOfPropability)*100)
                        propability.push {
                            id : edge.id
                            prop : test_prop
                        }
             
                    propability.sort (a,b)->
                        a.prop - b.prop

                    for i in [1...propability.length]
                        propability[i].prop += propability[i-1].prop
                    
                    randomEdge = propability[0] if propability.length isnt 0
                    for i in [1...propability.length]
                       if randomValue > propability[i-1].prop and randomValue <= propability[i].prop
                           randomEdge = propability[i]
                    if randomEdge isnt null
                       for edge in connectedEdges
                          if randomEdge.id is edge.id
                              randomEdge = edge
                else
                    randomEdge = connectedEdges[0]

                edgeVisited.push randomEdge
                if randomEdge.source is currentPoint
                    nextPoint = randomEdge.target
                else if randomEdge.target is currentPoint
                    nextPoint = randomEdge.source
             else
                if connectedEdges.length isnt 0
                    randomEdge = connectedEdges[Math.floor Math.random() * connectedEdges.length]
                    if currentPoint is randomEdge.target
                        nextPoint = randomEdge.source;
                    else
                        nextPoint = randomEdge.target;
                    edgeVisited.push randomEdge
                 else
                    return {
                        edges : edgeVisited
                        nodes : nodesVisited
                        length : 0
                    }
             nodesVisited.push currentPoint
             currentPoint = nextPoint
             break if currentPoint is @endPoint or connectedEdges.length is 0
        length_ = 0
        for edge in edgeVisited
            length_ += @edgeLengthList[edge.id]
        return {
            edges : edgeVisited
            nodes : nodesVisited
            length : length_
        }
                    

    startAlgorithm: (iloscMrowek,iloscIteracji,nagrodaFeromonowa=0.2,wplywFeromonu=3,wplywOdleglosci=1,wspolczynnikParowania = 0.5, feromonPoczatkowy=0,afterStartFunc = null)->
        errors = false
        if this.startPoint is null
            errors = true
            alertify.error "Brak węzła startowego"
        if this.endPoint is null
            errors = true
            alertify.error "Brak węzła końcowego"
        
        feromonPoczatkowy = parseFloat feromonPoczatkowy
        iloscMrowek = parseInt iloscMrowek
        iloscIteracji = parseInt iloscIteracji
        wspolczynnikParowania = parseFloat wspolczynnikParowania
        wplywFeromonu = parseFloat wplywFeromonu
        wplywOdleglosci = parseFloat wplywOdleglosci
        nagrodaFeromonowa = parseFloat nagrodaFeromonowa
        
        if errors is false
            this.stopForceAtlas()
            afterStartFunc() if afterStartFunc isnt null 
            if feromonPoczatkowy isnt 0
                for x,y of @feromoneTable
                    @feromoneTable[x] = feromonPoczatkowy
            bestSolution = null
            categories = [0...iloscIteracji]
            @highchartContainer.xAxis[0].setCategories categories,false
            for i in [0...iloscMrowek]
              @highchartContainer.addSeries {
                name: "Mrówka#{i}"
                data: []
              }
            
            @highchartContainer.redraw()
            for i in [0...iloscIteracji]
                solutionArray = new Array
                for x,y of @feromoneTable
                    @feromoneTable[x] = (1-wspolczynnikParowania)*@feromoneTable[x]
                
                for y in [0...iloscMrowek]
                    mr = this.antWalk(wplywFeromonu,wplywOdleglosci)
                    solutionArray.push mr if mr.length isnt 0
                    @highchartContainer.series[y].addPoint [i,mr.length],false
                for solution in solutionArray
                    if bestSolution is null or bestSolution.length > solution.length
                        bestSolution = solution
                for solution in solutionArray
                    for edge in solution.edges
                        @feromoneTable[edge.id] += 1/solution.length
                if bestSolution?
                    for edge in bestSolution.edges
                        @feromoneTable[edge.id] += nagrodaFeromonowa
            @highchartContainer.redraw()
            prevHid = []
            @sigInst.iterEdges (e)->
              for edge in bestSolution.edges
                if edge.id is e.id
                  prevHid.push e.id
               return
            @sigInst.iterEdges (e)->
              if prevHid.indexOf(e.id) is -1
                e.hidden = true
              else
                e.hidden = false
                e.color = '#f00'
            @sigInst.draw()
        @highchartContainer.redraw()
        return
        

    stopForceAtlas :->
        if @forceAtlasStarted
            @forceAtlasStarted = false
            @sigInst.stopForceAtlas2()
            @hookBeforeForceAtlas() if @hookBeforeForceAtlas isnt null
            this.recalculateEdgeValue()
            @hookAfterForceAtlas() if @hookAfterForceAtlas isnt null
        @hookAfterInvokeStopForceAtlas() if @hookAfterInvokeStopForceAtlas isnt null
        return

    startForceAtlas: ->
      @endPointSelectionStarted = false
      @startPointSelectionStarted = false
      @forceAtlasStarted = true
      @sigInst.startForceAtlas2()
      @hookAfterInvokeStartForceAtlas() if @hookAfterInvokeStartForceAtlas isnt null
      return
      
    clearGraph: ->
      @sigInst.emptyGraph()
      return

    generateGraph: (nodes,edges,clusters,d=0.5)->
      @startPoint = null
      @endPoint = null
      this.clearGraph()
      clusterList = []
      for i in [0...clusters]
        clusterList.push {
          id: i
          nodes: []
          color: "rgb(#{Math.round Math.random()*256},#{Math.round Math.random()*256},#{Math.round Math.random()*256})"
        }
      for i in [0...nodes]
        cluster = clusterList[Math.random()*clusters|0]
        @nodeList["n#{i}"] = {
          x : Math.random(),
          y : Math.random(),
          size : 0.5 + 4.5 * Math.random(),
          color : cluster['color']
          cluster : cluster['id']
        }
        @adjList["n#{i}"] = []
        cluster.nodes.push "n#{i}"
        @sigInst.addNode "n#{i}",@nodeList["n#{i}"]
      for i in [0...edges]
        if Math.random() < 1-d
          tmp_source = Math.random()*nodes|0
          tmp_destiny = Math.random()*nodes|0
          if tmp_source isnt tmp_destiny and @adjList["n#{tmp_source}"].indexOf("n#{tmp_destiny}") is -1
            @edgeLengthList["e#{i}"] = Math.sqrt( Math.pow(@nodeList["n#{tmp_source}"].x - @nodeList["n#{tmp_destiny}"].x,2)+Math.pow(@nodeList["n#{tmp_source}"].y - @nodeList["n#{tmp_destiny}"].y,2))
            @edgeList.push {
                id : "e#{i}",
                source: "n#{tmp_source}",
                target: "n#{tmp_destiny}"
            }
            @feromoneTable["e#{i}"]=0
            @sigInst.addEdge "e#{i}","n#{tmp_source}","n#{tmp_destiny}"
            @adjList["n#{tmp_source}"].push "n#{tmp_destiny}"
            @adjList["n#{tmp_destiny}"].push "n#{tmp_source}"
        else
          cluster = clusterList[Math.random()*clusters|0]
          n = cluster.nodes.length
          tmp_source = cluster.nodes[(Math.random()*clusters)|0]
          tmp_destiny = cluster.nodes[(Math.random()*clusters)|0]
          if tmp_source isnt tmp_destiny and @adjList[tmp_source].indexOf(tmp_destiny) is -1
            @edgeLengthList["e#{i}"] = Math.sqrt( Math.pow(@nodeList[tmp_source].x - @nodeList[tmp_destiny].x,2)+Math.pow(@nodeList[tmp_source].y - @nodeList[tmp_destiny].y,2))
            @sigInst.addEdge "e#{i}",tmp_source,tmp_destiny
            @edgeList.push {
                id : "e#{i}",
                source: tmp_source,
                target: tmp_destiny
            }
            @feromoneTable["e#{i}"] = 0
            
            @adjList[tmp_source].push tmp_destiny
            @adjList[tmp_destiny].push tmp_source
      @sigInst.draw()
      this.startForceAtlas() if @forceAtlasStarted
      return
$(".main").onepage_scroll();
$(".sterowanie a").click (e)->
    e.preventDefault()
    $(this).tab 'show'
    return
moje_aco = new aco "sigma-aco"
moje_aco.setBeforeForceAtlas ->
  $('#processing-modal').modal 'show'
  return

moje_aco.setAfterForceAtlas ->
  $('#processing-modal').modal 'hide'
  return

moje_aco.setAfterInvokeStopForceAtlas ->
    $('#stop-graph').html 'Wznów rozmieszczanie węzłów'
    return

moje_aco.setAfterInvokeStartForceAtlas ->
    $('#stop-graph').html 'Zatrzymaj rozmieszczanie węzłów'
    $('#select-start-node,#select-end-node').removeClass('btn-danger').addClass 'btn-info'
    return

moje_aco.generateGraph 100,500,5

moje_aco.getSigmaInstance().bind 'downnodes',(e)->
    moje_aco.click e
    alertify.success 'Wybrano punkt startowy' if moje_aco.statusStartPointSelection()
    alertify.success 'Wybrano punkt końcowy' if moje_aco.statusEndPointSelection()
    $('#start-point-info').html moje_aco.getStartNode() if moje_aco.getStartNode() isnt null
    $('#end-point-info').html moje_aco.getEndNode() if moje_aco.getEndNode() isnt null
    

$('#stop-graph').click (e) ->
    e.preventDefault()
    
    if moje_aco.isForceAtlasRunning()
      moje_aco.stopForceAtlas()
    else
      moje_aco.startForceAtlas()
      
    return
$('#generate-graph').click (e) ->
    e.preventDefault()
    moje_aco.generateGraph 100,500,5
    $('#start-point-info').html ''
    $('#end-point-info').html ''

$('#rescale-graph').click (e) ->
    e.preventDefault()
    moje_aco.rescaleGraph()
    
$('#select-start-node').click (e) ->
    e.preventDefault()
    if moje_aco.statusStartPointSelection()
        moje_aco.disableStartPointSelection()
        $(this).removeClass('btn-danger').addClass 'btn-info'
    else
        moje_aco.enableStartPointSelection()
        $(this).removeClass('btn-info').addClass 'btn-danger'
        $('#select-end-node').removeClass('btn-danger').addClass 'btn-info'
    return

$('#select-end-node').click (e) ->
    e.preventDefault()
    if moje_aco.statusEndPointSelection()
        moje_aco.disableEndPointSelection()
        $(this).removeClass('btn-danger').addClass 'btn-info'
    else
        moje_aco.enableEndPointSelection()
        $(this).removeClass('btn-info').addClass 'btn-danger'
        $('#select-start-node').removeClass('btn-danger').addClass 'btn-info'
        
$('#start-algorithm').click (e) ->
    e.preventDefault()
    moje_aco.startAlgorithm $('#iloscMrowek').val(),$('#iloscIteracji').val(),$('#nagrodaFeromonowa').val(),$('#wplywFeromonu').val(),$('#wplywOdleglosci').val(),$('#wspolczynnikParowania').val(),$('#feromonPoczatkowy').val(),()->
      $('.main').moveTo(2);